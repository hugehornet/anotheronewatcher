
/**
 * @module anotherOneWatcher
 */

"use strict";

var meta = require('./lib/util/meta');

module.exports = {
    /**
     * Stats storage object
     * @property {Object} Instrumenter
     * @static
     */
    store: require('./lib/store'),
    /**
     * @description VERSION
     * @property {String} VERSION
     * @static
     */
    VERSION: meta.VERSION
};