"use strict";


var assert = require("assert"),

    // module to be tested
    index = require("../index.js");


describe('anotherOneWatcher init', function(){
    describe('#version', function(){
        it('should return module version', function(){
            assert.deepEqual(
                index.VERSION,
                "0.0.1"
            );
        });
    });
});